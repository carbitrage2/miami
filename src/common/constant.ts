import constantData from '../constant.json';

function loadConstantData(): ConstantData {
    const data: ConstantData = constantData as ConstantData;
    return data;
}

async function areMarketsValid(received: string[], constant: string[]): Promise<boolean> {
    return new Promise(async resolve => {
        let result: boolean = true;
        for await (let market of received) {
            if (!(constant.includes(market))) {
                result = false;
            }
        }
        resolve(result);
    })
}

export { loadConstantData, areMarketsValid };