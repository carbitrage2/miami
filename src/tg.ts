import TelegramBot from 'node-telegram-bot-api';
import 'dotenv';

async function sendPassword(uid: number, password: string): Promise<void> {
    return new Promise(resolve => {
        const token: string = process.env.TG_TOKEN!;
        const bot: TelegramBot = new TelegramBot(token, {polling: false});
        bot.sendMessage(uid, `Your password: ${password}`);
        resolve();
    });
}

export { sendPassword };