async function createPassword(): Promise<string> {
    return new Promise(resolve => {
        let password: string = Math.floor(
            Math.random() * (9999 - 0) + 0
        ).toString();
        while (password.length < 4) {
            password = '0' + password;
        }
        resolve(password);
    });
}

async function createToken(): Promise<string> {
    return new Promise(resolve => {
        const symbols: string = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        const length: number = 48;
        let result: string = '';
        for (let i = 0; i < length; i++) {
            result += symbols.charAt(Math.floor(Math.random() * symbols.length));
        }
        resolve(result);
    });
}

export { createPassword, createToken };