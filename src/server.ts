import express, { Application } from 'express';
import bodyParser from 'body-parser';
import { createError, createSuccess, prepareSignalsMessage } from './common/responses';
import { createPassword, createToken } from './process';
import { 
    checkIfUserExists, 
    writePassword, 
    checkIfPasswordIsCorrect, 
    removePassword, 
    writeToken, 
    removeToken, 
    getUserID, 
    checkForNumberOfSignals, 
    createSignal, 
    getAllSignals, 
    deleteSignal, 
    getSpecificSignals
} from './db';
import { sendPassword } from './tg';
import cors from 'cors';
import { loadConstantData, areMarketsValid } from './common/constant';

async function startServer(): Promise<void> {
    const constantData: ConstantData = loadConstantData();
    const testUID: string | undefined = process.env.TEST_UID;
    const testPassword: string | undefined = process.env.TEST_PASSWORD;

    const app: Application = express();
    app.use(bodyParser.json(), cors());

    app.post('/getpass', async function (request: Request, response: any) {
        const receivedData: PasswordRequest = request.body as unknown as PasswordRequest;
        if (Object.keys(receivedData).length > 1) {
            response.status(400).send(createError('Wrong keys in request'));
        } else if (!("uid" in receivedData)) {
            response.status(400).send(createError('User ID key is missing'));
        } else if ("uid" in receivedData && (
            (receivedData.uid as Number && (receivedData.uid.toString().length > 12 || receivedData.uid.toString().length < 1) || 
            !(receivedData.uid as Number))
        )) {
            response.status(400).send(createError('Incorrect ID'))
        } else {
            if (receivedData.uid.toString() === testUID) {
                response.status(200).send(createSuccess('Password sent'));
            } else if (await checkIfUserExists(receivedData.uid.toString())) {
                await removeToken(receivedData.uid);
                const password: string = await createPassword();
                await writePassword(receivedData.uid, password);
                await sendPassword(receivedData.uid, password);
                response.status(200).send(createSuccess('Password sent'));
            } else {
                response.status(416).send(createError('User with this ID not exists'));
            }
        }
    });

    app.post('/auth', async function (request: Request, response: any) {
        const receivedData: AuthRequest = request.body as unknown as AuthRequest;
        if (Object.keys(receivedData).length > 2) {
            response.status(400).send(createError('Wrong keys in request'));
        } else if (!("uid" in receivedData) && !("password" in receivedData)) {
            response.status(400).send(createError('User ID key ans password are missing'));
        } else if (!("uid" in receivedData)) {
            response.status(400).send(createError('User ID key is missing'));
        } else if (!("password" in receivedData)) {
            response.status(400).send(createError('Password is missing'));
        } else if ("uid" in receivedData && (
            (receivedData.uid as Number && (receivedData.uid.toString().length > 12 || receivedData.uid.toString().length < 1) || 
            !(receivedData.uid as Number))
        )) {
            response.status(400).send(createError('Incorrect ID'))
        } else if ("password" in receivedData && (
            (receivedData.password as string && receivedData.password.length !== 4) || 
            !(receivedData.password as string))
        ) {
            response.status(400).send(createError('Incorrect password format'))
        } else {
            if (receivedData.uid.toString() === testUID && receivedData.password === testPassword) {
                const token: string = await createToken();
                await writeToken(receivedData.uid, token);
                response.status(200).send(createSuccess(token));
            } else {
                const authRes: string = await checkIfPasswordIsCorrect(receivedData);
                if (authRes === 'ok') {
                    const token: string = await createToken();
                    await writeToken(receivedData.uid, token);
                    await removePassword(receivedData.uid);
                    response.status(200).send(createSuccess(token));
                } else if (authRes === 'expired') {
                    response.status(416).send(createError('Password expired'));
                } else {
                    response.status(416).send(createError('Wrong User ID or password'));
                }
            }
        }
    });

    app.post('/signal', async function(request: any, response: any) {
        const receivedData: CreateSignalRequest = request.body as unknown as CreateSignalRequest;
        if (request.header('authorization')) {
            const token: string | undefined = request.header('authorization')!.split('Bearer ')[1];
            if (!(token as string) || token?.length !== 48) {
                response.status(401).send(createError('Invalid token'));
            } else {
                if (Object.keys(receivedData).length > 3) {
                    response.status(400).send(createError('Wrong keys in request'));
                } else if (!("pair" in receivedData)) {
                    response.status(400).send(createError('Pair is missing'));
                } else if (!(receivedData.pair as string) || !(constantData.pairs.includes(receivedData.pair))) {
                    response.status(400).send(createError('Invalid pair'));
                } else if (!("markets" in receivedData)) {
                    response.status(400).send(createError('Markets are missing'));
                } else if (!(receivedData.markets as string[]) || !(await areMarketsValid(receivedData.markets, constantData.markets))) {
                    response.status(400).send(createError('Invalid markets'));
                } else if (!("difference" in receivedData)) {
                    response.status(400).send(createError('Difference is missing'));
                } else if (!(receivedData.difference as number) || (receivedData.difference <= 0)) {
                    response.status(400).send(createError('Invalid difference'));
                } else if (receivedData.markets.length < 2) {
                    response.status(400).send(createError('At least two signals required to create difference'));
                } else {
                    const uid: number | null = await getUserID(token);
                    if (!uid) { response.status(400).send(createError('Wrong token')); }
                    else {
                        if (!(await checkForNumberOfSignals(uid))) {
                            response.status(400).send(createError('Too many signals for single user'));
                        } else {
                            const sID: number | null = await createSignal(uid, receivedData);
                            if (sID) {
                                response.status(200).send(createSuccess(sID.toString()));
                            } else {
                                response.status(400).send(createError('Signal ID is not available'));
                            }
                        }
                    }
                }
            }
        } else {
            response.status(401).send(createError('Authorization token is missing'));
        }
    });

    app.get('/signals', async function(request: any, response: any) {
        if (request.header('authorization')) {
            const token: string | undefined = request.header('authorization')!.split('Bearer ')[1];
            if (!(token as string) || token?.length !== 48) {
                response.status(401).send(createError('Invalid token'));
            } else {
                const uid: number | null = await getUserID(token);
                if (!uid) {
                    response.status(400).send(createError('Wrong token'));
                } else {
                    if (Object.keys(request.query).length !== 0) {
                        const params: GetSignalsParams = request.query as unknown as GetSignalsParams;
                        if (!("pair" in params) || !(constantData.pairs.includes(params.pair))) {
                            response.status(400).send(createError('Invalid pair parameter'));
                        } else if (!("markets" in params) || !(await areMarketsValid(params.markets.split(','), constantData.markets))) {
                            response.status(400).send(createError('Invalid markets parameter'));
                        } else if (Object.keys(params).length > 2) {
                            response.status(400).send(createError('Wrong keys in request'));
                        } else {
                            const signals: SpecificSignal[] | [] = await getSpecificSignals(uid, params);
                            response.status(200).send(prepareSignalsMessage(signals));
                        }
                    } else {
                        const signals: AnySignal[] | [] = await getAllSignals(uid);
                        response.status(200).send(prepareSignalsMessage(signals));
                    }
                }
            }
        } else {
            response.status(401).send(createError('Authorization token is missing'));
        }
    });

    app.delete('/signal', async function(request: any, response: any) {
        const receivedData: DeleteSignalRequest = request.body as DeleteSignalRequest;
        const sid: number = receivedData.sid;
        if (request.header('authorization')) {
            const token: string | undefined = request.header('authorization')!.split('Bearer ')[1];
            if (!(token as string) || token?.length !== 48) {
                response.status(401).send(createError('Invalid token'));
            } else {
                const uid: number | null = await getUserID(token);
                if (!uid) {
                    response.status(400).send(createError('Wrong token'));
                } else {
                    if (await deleteSignal(uid, sid)) {
                        response.status(200).send(createSuccess('Signal deleted'));
                    } else {
                        response.status(400).send(createError(`Couldn't delete signal`));
                    }
                }
            }
        } else {
            response.status(401).send(createError('Authorization token is missing'));
        }
    });

    app.get('/user-data', async function(request: any, response: any) {
        if (request.header('authorization')) {
            const token: string | undefined = request.header('authorization')!.split('Bearer ')[1];
            if (!(token as string) || token?.length !== 48) {
                response.status(401).send(createError('Invalid token'));
            } else {
                const uid: number | null = await getUserID(token);
                if (uid) {
                    response.status(200).send(createSuccess(uid.toString()));
                } else {
                    response.status(400).send(createError('Wrong token'));
                }
            }
        } else {
            response.status(401).send(createError('Authorization token is missing'));
        }
    });

    app.listen(+process.env.API_PORT!, process.env.API_HOST!, function () {
        console.log(`App is listening on port ${process.env.API_PORT}!`);
    });
}

export { startServer };