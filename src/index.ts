import { config as dotenvConfig } from 'dotenv';
import { startServer } from './server';
import { loadConstantData } from './common/constant';

function startResting() {
    dotenvConfig();
    loadConstantData();
    startServer();
}

startResting();