declare interface ConstantData {
    pairs: string[];
    markets: string[];
}

declare interface AnySignal {
    sid: number;
    pair: string;
    markets: string[];
    difference: number;
}

declare interface SpecificSignal {
    sid: number;
    value: number;
    label: number;
}

declare interface GetSignalsParams {
    pair: string;
    markets: string;
}