declare interface PasswordRequest {
    uid: number;
}

declare interface AuthRequest {
    uid: number;
    password: string;
}

declare interface CreateSignalRequest {
    pair: string;
    markets: string[];
    difference: number;
}

declare interface DeleteSignalRequest {
    sid: number;
}